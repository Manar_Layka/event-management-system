from wagtail.contrib.modeladmin.options import modeladmin_register, ModelAdmin
from .models import BlogPage


class BlogPageAdmin(ModelAdmin):
    model = BlogPage
    menu_label = "Blog Page"
    menu_icon = "pick"
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("intro", )
    list_filter = ("author", )
    search_fields = ("intro", )


modeladmin_register(BlogPageAdmin)