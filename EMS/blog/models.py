from django.db import models
from wagtail.core import blocks
from wagtail.core.models import Page, Orderable
from wagtail.core.blocks import StructBlock
from taggit.models import TaggedItemBase
from wagtail.documents.models import AbstractDocument, Document
from modelcluster.models import ClusterableModel
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.search import index
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager, TaggableManager
from wagtail.snippets.models import register_snippet
from wagtail.snippets.edit_handlers import SnippetChooserPanel

# Create your models here.


('person', blocks.StructBlock([
    ('first_name', blocks.CharBlock()),
     ('surname', blocks.CharBlock()),
     ('photo', ImageChooserBlock(required=False)),
     ('biography', blocks.RichTextBlock())
], icon='user',
    template='blog/blocks/person.html'))


class PersonBlock(blocks.StructBlock):
    first_name = blocks.CharBlock()
    surname = blocks.CharBlock()
    photo = ImageChooserBlock(required=False)
    biography = blocks.RichTextBlock()

    class Meta:
        icon = 'user'
        template = 'blog/blocks/person.html'


class BlogIndexPage(Page):
    Intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('Intro', classname="full"),
    ]


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'BlogPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class BlogPage(Page, index.Indexed):
    date = models.DateField("Post Date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)
    author = StreamField([
        ('name', blocks.CharBlock(form_classname="full name")),
        ('image', ImageChooserBlock()),
        ('drink', blocks.ChoiceBlock(choices=[
            ('tea', 'Tea'),
            ('coffee', 'Coffee'),
        ], icon='cup')),
        ('person', PersonBlock())
    ], blank=True)
    advert = models.ForeignKey('Advert', on_delete=models.CASCADE, blank=True, null=True)
    # advert_placements = models.ForeignKey('PageAdvertPlacement', on_delete=models.CASCADE, blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('intro', partial_match=True, boost=2),
        index.SearchField('body', boost=1),
        index.FilterField('date'),
        # index.RelatedField('related_links', [
        #     index.SearchField('image'),
        # ]),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('tags'),
            ], heading="Blog Information"),
        FieldPanel('date'),
        FieldPanel('intro'),
        FieldPanel('body', classname="full"),
        StreamFieldPanel('author'),
        InlinePanel('gallery_images', label="Gallery images"),
        InlinePanel('related_links', label="related links"),
        SnippetChooserPanel('advert'),
        InlinePanel("advert_placements", label="Adverts")
    ]

    def get_context(self, request):
        tag = request.GET.get('tag')
        blog_pages = BlogPage.objects.filter(tags__name=tag)
        context = super().get_context(request)
        context['blog_pages'] = blog_pages
        return context


class BlogPageGalleryImage(Orderable):
    page = ParentalKey(BlogPage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


class BlogPageRelatedLing(Orderable):
    page = ParentalKey(BlogPage, on_delete=models.CASCADE, related_name='related_links')
    name = models.CharField(max_length=255)
    url = models.URLField(null=True, blank=True)

    panels = [
        FieldPanel('name'),
        FieldPanel('url')
    ]


class AdvertTag(TaggedItemBase):
    content_object = ParentalKey('Advert', on_delete=models.CASCADE, related_name="tagged_items")


@register_snippet
class Advert(ClusterableModel):
    url = models.URLField(null=True, blank=True)
    text = models.CharField(max_length=255)
    tags = TaggableManager(through=AdvertTag, blank=True)

    panels = [
        FieldPanel('url'),
        FieldPanel('text'),
        FieldPanel('tags'),
    ]

    def __str__(self):
        return self.text


class PageAdvertPlacement(Orderable):
    page = ParentalKey('BlogPage', on_delete=models.CASCADE, related_name='advert_placements')
    advert = models.ForeignKey('Advert', on_delete=models.CASCADE, related_name="+")

    class Meta(Orderable.Meta):
        verbose_name = "advert placement"
        verbose_name_plural = "advert placements"

    panels = [
        SnippetChooserPanel('advert')
    ]

    def __str__(self):
        return self.page.title + " -> " + self.advert.text


class CustomDocument(AbstractDocument):
    source = models.CharField(
        max_length=255,
        blank=True,
        null=True,
    )

    admin_form_fields = Document.admin_form_fields + (
        'source',
    )