from django import forms
from events.models import Event, Location
from django.utils.html import format_html, escape, mark_safe
from string import Template


class PictureWidget(forms.widgets.Widget):
    def render(self, name, value, attrs=None, **kwargs):
        html =  Template("""<img src="$link"/>""")
        return mark_safe(html.substitute(link=value))


class EventForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    # Image = forms.ImageField(widget=PictureWidget)

    class Meta:
        model = Event
        fields = ('name', 'details','description_ar', 'date', 'time', 'image',)
        # widgets = {
        #     'image' : forms.ImageField({'is_hidden' : False})
        # }


class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ('name', 'event_location', 'image', )