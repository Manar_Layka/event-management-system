from django.contrib import admin
from events.models import Location, Event, Located, EventImage, LocationImage, DraftEvent, PublishedEvent,\
    WithdrawnEvent, DraftLocation, PublishedLocation, WithdrawnLocation
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_permission_codename
from django.utils.translation import gettext_lazy as _


# Register your models here.


class LocatedInline(admin.TabularInline):
    model = Located
    extra = 1


class LocationAdmin(admin.ModelAdmin):

    fieldsets = [
        ('Location Name', {'fields': ['name']}),
        ('Status', {'fields': ['status']}),
        ('Publish date', {'fields': ['published_date']}),
        ('Creator', {'fields': ['creator']}),
        ('Events', {'fields': ['events_names']}),
        ('Image', {'fields': ['image_tag']}),
    ]
    inlines = (LocatedInline,)
    readonly_fields = ('image_tag', 'events_names',)

    list_display = ('colored_location_name', 'status', 'events_names')
    actions = ['make_published']

    def make_published(self, request, pk):
        pk.update(status='published')

    make_published.allowed_permissions = ('publish',)

    def has_publish_permission(self, request):
        opts = self.opts
        codename = get_permission_codename('publish', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))


class EventsAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Event Name', {'fields': ['name']}),
        ('date', {'fields': ['date']}),
        ('Status', {'fields': ['status_en', 'status_ar']}),
        # ('Status', {'fields': ['status_en']}),
        ('Creator', {'fields': ['creator']}),
        ('Publish date', {'fields': ['published_date']}),
        # ('Event Description', {'fields': ['event_description']}),
        ('Arabic Event Description', {'fields':['description_ar']}),
        ('English Event Description', {'fields': ['description_en']}),
        ('Image', {'fields': ['image_tag']}),
    ]
    inlines = (LocatedInline,)
    # readonly_fields = ('thumbnail_preview', )
    readonly_fields = ('image_tag',)

    # def thumbnail_preview(self, obj):
    #     return obj.thumbnail_preview
    #
    # thumbnail_preview.short_description = 'Thumbnail Preview'
    # thumbnail_preview.allow_tags = True

    list_display = ('colored_event_name', 'date', 'status')

    actions = ['make_published']

    def make_published(self, request, pk):
        pk.update(status_en='published')
        pk.update(status_ar='منشورة')

    make_published.allowed_permissions = (_('publish'),)

    def has_publish_permission(self, request):
        opts = self.opts
        codename = get_permission_codename('publish', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))


# class LocatedAdmin(admin.ModelAdmin):
#     fieldsets = [
#         ('Event Type', {'fields': ['event_type']}),
#         ('location', {'fields': ['location']}),
#         ('events', {'fields': ['event']}),
#     ]
#     list_display = ('event_type', 'location', 'event')

class EventImageAdmin(admin.ModelAdmin):
    fields = ('image_tag',)
    readonly_fields = ('image_tag',)


class LocationImageAdmin(admin.ModelAdmin):
    fields = ('image_tag',)
    readonly_fields = ('image_tag',)


admin.site.register(Location, LocationAdmin)
admin.site.register(Event, EventsAdmin)
admin.site.register(Located)
admin.site.register(EventImage, EventImageAdmin)
admin.site.register(LocationImage, LocationImageAdmin)
admin.site.register(DraftEvent)
admin.site.register(PublishedEvent)
admin.site.register(WithdrawnEvent)
admin.site.register(DraftLocation)
admin.site.register(PublishedLocation)
admin.site.register(WithdrawnLocation)
