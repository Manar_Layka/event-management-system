from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.utils.translation import get_language
from translated_fields import TranslatedField
from django.utils.html import format_html, mark_safe, escape
from django.utils import timezone
from django.shortcuts import get_object_or_404, reverse
from djrichtextfield.models import RichTextField
from django import forms
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit


from wagtail.core.models import Page, Orderable
from taggit.models import TaggedItemBase
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager

# Create your models here.


STATUS_CHOICES = [
    ('draft', _('Draft')),
    ('published', _('Published')),
    ('withdrawn', _('Withdrawn')),
]

preview_size = 1000


class EventWagtail(Page):
    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    time = models.TimeField(null=True)
    published_date = models.DateTimeField(blank=True, null=True)
    event_details = models.CharField(max_length=300, blank=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Draft')
    # creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL)
    event_description = RichTextField()
    # image = models.FileField(upload_to='event_images', blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('name'),
        FieldPanel('date'),
        FieldPanel('time'),
        FieldPanel('event_description', classname="full"),
        InlinePanel('gallery_images', label="Gallery images"),
    ]


class EventPageGalleryImage(Orderable):
    page = ParentalKey(EventWagtail,  on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank= True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


class Event(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    time = models.TimeField(null=True)
    published_date = models.DateTimeField(blank=True, null=True)
    details = models.CharField(max_length=300, blank=True)
    # details = TranslatedField(models.CharField(_("details"), max_length=300, blank=True))
    status = models.CharField(max_length=10)
    status = TranslatedField(models.CharField(_("status"), max_length=10, choices=STATUS_CHOICES, default='Draft'))
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    description = RichTextField()
    description = TranslatedField(RichTextField(_("description"), blank=True))
    image = models.FileField(upload_to='event_images', blank=True)

    # image = models.ManyToManyField(EventImage, through='EventImage')

    def get_description(self):
        l = get_language()
        if l == 'en-us':
            self.description = self.description.fields[0]
            self.details = self.details.fields[0]
            self.status = self.status.fields[0]
        else:
            self.description = self.description.fields[1]
            self.details = self.details.fields[1]
            self.status = self.status.fields[1]

    def __unicode__(self):
        return self.image.url

    # @property
    # def thumbnail_preview(self):
    #     if self.image:
    #         return mark_safe('<img src="{}" width="300" height="300" />'.format(self.image.url))
    #     return ""

    def get_absolute_url(self):
        return reverse('events:Event', kwargs={'pk': self.pk})

    def colored_event_name(self):
        return format_html(
            '<span style="color: #42b8bb;">{}</span>',
            self.name,
        )

    def image_tag(self):
        return format_html(u'<img src="%s" />' % escape(self.image.url))

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Event'
        verbose_name_plural = 'Events'
        ordering = ['date', 'time']


class DraftManager(models.Manager):
    def get_queryset(self):
        return super(DraftManager, self).get_queryset().filter(status='draft')


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')


class WithdrawnManager(models.Manager):
    def get_queryset(self):
        return super(WithdrawnManager, self).get_queryset().filter(status='withdrawn')


class DraftEvent(Event):
    objects = DraftManager()

    class Meta:
        proxy = True


class PublishedEvent(Event):
    objects = PublishedManager()

    class Meta:
        proxy = True


class WithdrawnEvent(Event):
    objects = WithdrawnManager()

    class Meta:
        proxy = True


class EventImage(models.Model):
    image = models.ImageField(upload_to='event_images')
    image_preview = ImageSpecField(source='image',
                                   processors=[ResizeToFit(width=preview_size, height=preview_size)],
                                   format='JPEG',
                                   options={'quality': 60}

                                   )
    event = models.ManyToManyField(Event, blank=True)

    def image_tag(self):
        return format_html(u'<img src="%s" />' % escape(self.image.url))

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True


class Location(models.Model):
    name = models.CharField(max_length=100)
    event_location = models.ManyToManyField(Event, through='Located')
    published_date = models.DateTimeField(blank=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Draft')
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='location_images', blank=True)

    def colored_location_name(self):
        return format_html(
            '<span style="color: #42b8bb;">{}</span>',
            self.name,
        )

    def image_tag(self):
        return format_html(u'<img src="%s" />' % escape(self.image.url))

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def events_names(self):
        return ', '.join([a.name for a in self.event_location.all()])

    events_names.short_description = "event_names"
    events_names.allow_tags = True

    def __str__(self):
        return self.name


class DraftLocationManager(models.Manager):
    def get_queryset(self):
        return super(DraftLocationManager, self).get_queryset().filter(status='draft')


class PublishedLocationManager(models.Manager):
    def get_queryset(self):
        return super(PublishedLocationManager, self).get_queryset().filter(status='published')


class WithdrawnLocationManager(models.Manager):
    def get_queryset(self):
        return super(WithdrawnLocationManager, self).get_queryset().filter(status='withdrawn')


class DraftLocation(Location):
    objects = DraftLocationManager()

    class Meta:
        proxy = True


class PublishedLocation(Location):
    objects = PublishedLocationManager()

    class Meta:
        proxy = True


class WithdrawnLocation(Location):
    objects = WithdrawnLocationManager()

    class Meta:
        proxy = True


class LocationImage(models.Model):
    image = models.ImageField(upload_to='locations_images')
    image_preview = ImageSpecField(source='image',
                                   processors=[ResizeToFit(width=preview_size, height=preview_size)],
                                   format='JPEG',
                                   options={'quality': 60}

                                   )
    location = models.ManyToManyField(Location, blank=True)

    def image_tag(self):
        return format_html(u'<img src="%s" />' % escape(self.image.url))

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True


class Located(models.Model):
    event_type = models.CharField(max_length=30)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, blank=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True)
