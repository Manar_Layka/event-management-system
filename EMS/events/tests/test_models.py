from django.test import TestCase
from events.models import Event, Location, Located
from django.utils import timezone
from django.contrib.auth.models import User
from model_bakery import baker


def test_location_model(location, event, user):
    assert location.location_name == "Lattakia"
    assert location.event_location == event
    assert location.publish_date == timezone.now()
    assert location.status == "Draft"
    assert location.creator == user


def test_event_model(event, user):
    assert event.event_name == "ACM"
    # assert event.event_details == "Competition"
    assert event.status == "Draft"
    assert event.creator == user


def test_located_model(located):
    assert located.event_type == "Educational"


def test_location_label(location):
    assert location._meta.get_field('location_name') == 'location_name'
    assert location._meta.get_field('event_location') == 'event_location'
    assert location._meta.get_field('publish_date') == 'publish_date'
    assert location._meta.get_field('status') == 'status'
    assert location._meta.get_field('creator') == 'creator'


def test_event_label(event):
    assert event._meta.get_field('event_name') == 'event_name'
    assert event._meta.get_field('event_details') == 'event_details'
    assert event._meta.get_field('status') == 'status'
    assert event._meta.get_field('date') == 'date'
    assert event._meta.get_field('time') == 'time'
    assert event._meta.get_field('creator') == 'creator'


def test_located_label(located):
    assert located._meta.get_field('event_type') == 'event_type'
    assert located._meta.get_field('location') == 'location'
    assert located._meta.get_field('event') == 'event'


def test_field_attributes(event):
    assert event._meta.get_field('event_name').max_length == 100
    assert event._meta.get_field('event_details').max_length == 300


class EventTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create_user(username='manar1', password=12345, email='manar.layka@kuwaitnet.com' )
        Event.objects.create(event_name='ACM', status='Draft', creator=user, date='2018-05-18', time='12:25:00',
                             event_details='soon')

    # def test_event_name_label(self):
    #     event = Event.objects.get(id=1)
    #     field_label = event._meta.get_field('name').verbose_name
    #     self.assertEqual(field_label, 'name')

    def test_event_details_label(self):
        event = Event.objects.get(id=1)
        field_label = event._meta.get_field('event_details').verbose_name
        self.assertEqual(field_label, 'event_details')

    # def test_date_label(self):
    #     event = Event.objects.get(id=1)
    #     field_label = event._meta.get_field('date').verbose_name
    #     self.assertEqual(field_label, 'date')
    #
    # def test_time_label(self):
    #     event = Event.objects.get(id=1)
    #     field_label = event._meta.get_field('time').verbose_name
    #     self.assertEqual(field_label, 'time')
    #
    # def test_status_label(self):
    #     event = Event.objects.get(id=1)
    #     field_label = event._meta.get_field('status').verbose_name
    #     self.assertEqual(field_label, 'status')
    #
    # def test_creator_label(self):
    #     event = Event.objects.get(id=1)
    #     field_label = event._meta.get_field('creator').verbose_name
    #     self.assertEqual(field_label, 'creator')

    def test_event_name_max_length(self):
        event = Event.objects.get(id=1)
        max_length = event._meta.get_field('name').max_length
        self.assertEqual(max_length, 100)

    def test_event_details_max_length(self):
        event = Event.objects.get(id=1)
        max_length = event._meta.get_field('event_details').max_length
        self.assertEqual(max_length, 300)

    def test_event_creation(self):
        event = Event.objects.get(id=1)
        self.assertTrue(isinstance(event, Event))
        self.assertEqual(event.__unicode__(), event.name)

    def test_get_absolute_url(self):
        event = Event.objects.get(id=1)
        self.assertEqual(event.get_absolute_url(), 'events/')

    def test_event_name(self):
        event = Event.objects.get(id=1)
        expected_event_name = event.name
        self.assertEqual(expected_event_name, str(event))


class LocationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create_user(username='manar1', password=12345, email='manar.layka@kuwaitnet.com')
        event = Event.objects.create(event_name='ACM', status='Draft', creator=user, date='2018-05-18', time='12:25:00',
                             event_details='soon')
        Location.objects.create(location_name='Lattakia',event_location=event, creator=user)

    def test_location_name_label(self):
        location = Location.objects.get(id=1)
        field_label = location._meta.get_field('location_name').verbose_name
        self.assertEqual(field_label, 'location_name')

    def test_event_location_label(self):
        location = Location.objects.get(id=1)
        field_label = location._meta.get_field('event_location').verbose_name
        self.assertEqual(field_label, 'event_location')

    def test_publish_date_label(self):
        location = Location.objects.get(id=1)
        field_label = location._meta.get_field('publish_date').verbose_name
        self.assertEqual(field_label, 'publish_date')

    def test_status_label(self):
        location = Location.objects.get(id=1)
        field_label = location._meta.get_field('status').verbose_name
        self.asserEqual(field_label, 'status')

    def test_creator_label(self):
        location = Location.objects.get(id=1)
        field_label = location._meta.get_field('creator').verbose_name
        self.assertEqual(field_label, 'creator')


class LocatedTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Located.objects.create(event_type='Educational')

    def test_event_type_label(self):
        located = Located.objects.get(id=1)
        field_label = located._meta.get_field('event_type').verbose_name
        self.assertEqual(field_label, 'event_type')

    def test_location_label(self):
        located = Located.objects.get(id=1)
        field_label = located._meta.get_field('location').verbose_name
        self.assertEqual(field_label, 'location')

    def test_event_label(self):
        located = Located.objects.get(id=1)
        field_label = located._meta.get_field('event').verbose_name
        self.assertEqual(field_label, 'event')







