from django.test import TestCase
from django.utils import timezone
from django.shortcuts import reverse
from events.models import Event, Location, Located
from django.contrib.auth.models import User
import datetime
# Create your tests here.


# class LocationViewTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         number_of_locations = 13
#
#         user = User.objects.create_user(username='manar1', password=12345, email='manar.layka@kuwaitnet.com')
#         event = Event.objects.create(event_name='ACM', status='Draft', creator=user, date='2018-05-18', time='12:25:00',
#                              event_details='soon')
#
#         for location_id in range(number_of_locations):
#             Location.objects.create(location_name=f'Lattakia{location_id}', event_location=event, creator=user)


def create_event(event_name,days, event_details, status):
    date = timezone.now() + datetime.timedelta(days=days)
    return Event.objects.create(name=event_name, date=date, event_details=event_details, status=status)


class EventViewTest(TestCase):
    def test_no_event(self):
        response = self.client.get(reverse('events:events'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No events are available")
        self.assertQuerysetEqual(response.context['events_list'], [])