from django.urls import path, include
from events import views

app_name = 'events'
urlpatterns = [
   # path('', views.login_view, name='login'),
   path('', include('users.urls')),
   path('home/', views.IndexView.as_view(), name='index'),
   path('locations/', views.LocationView.as_view(), name='locations'),
   path('events/', views.EventView.as_view(), name='events'),
   path('locations/<pk>/locations_details/', views.location_detail, name='locations_details'),
   path('events/<pk>/events_details/', views.event_detail, name='events_details'),
   path('event/add/', views.EventCreate.as_view(), name='event-add'),
   path('event/<pk>/update', views.EventUpdate.as_view(), name='event-update'),
   path('location/add', views.LocationCreate.as_view(), name='locations-add'),
   path('events/drafted_events/', views.DraftEventView.as_view(), name='drafted-events'),
   path('events/published_events/', views.PublishedEventView.as_view(), name='published_events'),
   path('events/withdrawn_events/', views.WithdrawnLocationView.as_view(), name='withdrawn_location'),
   path('locations/drafted_locations/', views.DraftLocationView.as_view(), name='drafted_locations'),
   path('locations/published_locations/', views.PublishLocationView.as_view(), name='published_locations'),
   path('locations/withdrawn_locations/', views.WithdrawnLocationView.as_view(), name='withdrawn_locations'),
   path('search_results/', views.SearchResultsView.as_view(), name='search_results'),
   # path('locations/<pk>/publish/', views.location_publish, name='location_publish'),
]