from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from .models import Location, Event, EventImage, LocationImage, DraftEvent, PublishedEvent, WithdrawnEvent, \
    DraftLocation, PublishedLocation, WithdrawnLocation
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from events.forms import EventForm, LocationForm
from itertools import chain
from django.urls import reverse_lazy
from django.db.models import Q


# from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth import login
# Create your views here.


class EventView(generic.ListView):
    template_name = 'events.html'
    context_object_name = 'events_list'

    def get_queryset(self):
        return Event.objects.order_by('date')[:5]


class DraftEventView(generic.ListView):
    template_name = 'draft_events.html'
    context_object_name = 'draft_events'

    def get_queryset(self):
        return DraftEvent.objects.all()


class WithdrawnEventView(generic.ListView):
    template_name = 'withdrawn_events.html'
    context_object_name = 'withdrawn_events'

    def get_queryset(self):
        return WithdrawnEvent.objects.all()


class PublishedEventView(generic.ListView):
    template_name = 'published_events.html'
    context_object_name = 'published_events'

    def get_queryset(self):
        return PublishedEvent.objects.all()


class LocationView(generic.ListView):
    template_name = 'locations.html'
    context_object_name = 'locations_list'

    def get_queryset(self):
        return Location.objects.all()[:5]


class DraftLocationView(generic.ListView):
    template_name = 'drafted_locations.html'
    context_object_name = 'drafted_locations'

    def get_queryset(self):
        return DraftLocation.objects.all()


class PublishLocationView(generic.ListView):
    template_name = 'published_locations.html'
    context_object_name = 'published_locations'

    def get_queryset(self):
        return PublishedLocation.objects.all()


class WithdrawnLocationView(generic.ListView):
    template_name = 'withdrawn_locations.html'
    context_object_name = 'withdrawn_locations'

    def get_queryset(self):
        return WithdrawnLocation.objects.all()


class IndexView(generic.ListView):
    template_name = 'base.html'
    context_object_name = 'locations_list'

    def get_queryset(self):
        return Location.objects.all()


class EventCreate(CreateView):
    # model = Event
    # fields = ('name', 'event_details', 'date', 'time', 'image', )
    template_name = "events/event_form.html"
    context_object_name = 'event'
    success_message = "%(name)s was created successfully"

    form_class = EventForm

    def form_valid(self, form):
        form.instance.creator = self.request.user
        form.instance.image = self.request.FILES['image']
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('events:events'))
        else:
            render(self.request, 'events/event_form.html', {'form': form})

    # def get(self, request, *args, **kwargs):
    #     context = {'form': EventForm()}
    #     return render(request, 'events/event_form.html', context)
    #
    # def post(self, request, *args, **kwargs):
    #     form = EventForm(request.POST, request.FILES)
    #     if form.is_valid():
    #         form.save()
    #         return HttpResponseRedirect(reverse_lazy('events:events'))
    #     return render(request, 'events/event_form.html', {'form': form})


class EventUpdate(UpdateView):
    model = Event
    fields = ('name', 'details', 'date', 'time', 'creator','description')
    template_name = "events/event_form.html"
    context_object_name = 'event'

    def form_valid(self, form):
        form.instance.creator = self.request.user
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('events:events'))
        else:
            render(self.request, 'events/event_form.html', {'form': form})

    # def get(self, request, *args, **kwargs):
    #     context = {'form': EventForm()}
    #     return render(request, 'events/event_form.html', context)
    #
    # def post(self, request, *args, **kwargs):
    #     form = EventForm(request.POST)
    #     if form.is_valid():
    #         event = form.save()
    #         return HttpResponseRedirect(reverse_lazy('events:events'))
    #     return render(request, 'events/event_form.html', {'form' : form})


class LocationCreate(CreateView):
    model = Location
    form_class = LocationForm
    template_name = "events/location_form.html"
    context_object_name = 'location'

    def get(self, request, *args, **kwargs):
        context = {'form': LocationForm()}
        return render(request, 'events/location_form.html', context)


def location_detail(request, pk):
    location = Location.objects.get(pk=pk)
    return render(request, 'locations_details.html', {'location': location})


def event_detail(request, pk):
    event = Event.objects.get(pk=pk)
    return render(request, 'events : events_details', event)


class EventView(generic.ListView):
    template_name = 'events.html'
    context_object_name = 'events_list'

    def get_queryset(self):
        return Event.objects.order_by('date')[:5]

# @user_passes_test(lambda user: user.is_superuser)
# @login_required
# # @permission_required('events.')
# def location_publish(request, pk):
#     # is_superuser = request.user.is_superuser
#     # if not is_superuser:
#     #     return
#     location = get_object_or_404(Location, pk=pk)
#     location.publish()
#     location_publish.allowed_permissions = ('publish', )
#     return redirect('/locations/')


class SearchResultsView(generic.ListView):
    template_name = 'search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        event_objects = Event.objects.filter(
            Q(name__icontains=query)
        )
        location_objects = Location.objects.filter(
            Q(name__icontains=query)
        )
        return event_objects, location_objects
