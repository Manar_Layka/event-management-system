from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = UserCreationForm.Meta.fields + ("email",)

        class Meta:
            model = User

        # ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')